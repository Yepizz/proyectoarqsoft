﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ComputerSales
{
    public partial class FrmVentas : Form
    {
        public FrmVentas()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            //Cierra la ventana actual
            if (MessageBox.Show("¿Desea salir?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {//Buscar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "SELECT * FROM ventas where id_cliente like '%" + txtBuscar.Text.Trim() + "%' limit 50";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            dgRegistrosVenta.Rows.Clear();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dgRegistrosVenta.Rows.Add(reader.GetString(0), reader.GetString(1),
                            reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                    }
                }
                else
                {
                    MessageBox.Show("No se encontraron datos.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (dgRegistrosVenta.Rows.Count >= 50)
                MessageBox.Show("Se encontraron demasiados registros.\nNo se muestran todos los registros encontados.",
                    "Debes depurar la búsqueda");
        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {
            dgRegistrosVenta.Columns.Add("id_venta", "ID Venta");
            dgRegistrosVenta.Columns.Add("id_cliente", "ID Cliente");
            dgRegistrosVenta.Columns.Add("id_laptop", "ID Laptop");
            dgRegistrosVenta.Columns.Add("fecha", "Fecha");
            dgRegistrosVenta.Columns.Add("cantidad", "Cantidad");
            dgRegistrosVenta.Columns.Add("precio_venta", "Precio Venta");

        }

        private void dgRegistrosVenta_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIdVenta.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtIdCliente.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtIdLaptop.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtFecha.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtCantidad.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtPrecio.Text = dgRegistrosVenta.Rows[e.RowIndex].Cells[5].Value.ToString();

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Agregar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "INSERT INTO ventas VALUES(NULL,'" + txtIdCliente.Text + "','" + txtIdLaptop.Text + "','"
                    + txtFecha.Text.Substring(6, 4) + txtFecha.Text.Substring(3, 2) + txtFecha.Text.Substring(0, 2)
                    + txtFecha.Text.Substring(11, 2) + txtFecha.Text.Substring(14, 2) + txtFecha.Text.Substring(17, 2)
                    + "','" + txtCantidad.Text + "','" + txtPrecio.Text + "')";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader;
                limpiarForms();
            try
                {
                    databaseConnection.Open();
                    reader = commandDatabase.ExecuteReader();
                    databaseConnection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                btnBuscar_Click(sender, e); //Buscar
            
        }

        public void limpiarForms()
        {
            txtIdLaptop.Text = "";
            txtCantidad.Text = "";
            txtFecha.Text = "";
            txtIdCliente.Text = "";
            txtIdVenta.Text = "";
            txtPrecio.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar esta venta?", "Borrar venta", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {


                //Eliminar
                string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "DELETE FROM ventas where id_venta=" + txtIdVenta.Text;
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            btnBuscar_Click(sender, e); //Buscar
           }
            else MessageBox.Show("Error la venta NO ha sido eliminada");

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Modificar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "UPDATE ventas SET id_cliente='"
                + txtIdCliente.Text.Trim() + "', id_laptop='"
                + txtIdLaptop.Text.Trim() + "', fecha='"
                + txtFecha.Text.Substring(6, 4) + txtFecha.Text.Substring(3, 2) + txtFecha.Text.Substring(0, 2)
                + txtFecha.Text.Substring(11, 2) + txtFecha.Text.Substring(14, 2) + txtFecha.Text.Substring(17, 2)
                + "', cantidad='" + txtCantidad.Text.Trim()
                + "', precio_venta='" + txtPrecio.Text.Trim()
                + "' WHERE id_venta=" + txtIdVenta.Text;
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            limpiarForms();
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
        }

    }
}
