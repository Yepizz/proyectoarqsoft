﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComputerSales
{
    public partial class FrmClientes : Form
    {
        public FrmClientes()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            //Cierra la ventana actual
            if (MessageBox.Show("¿Desea salir?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //Buscar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "SELECT * FROM clientes where nombre like '%" + txtBuscar.Text.Trim() + "%' limit 50";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            dgRegistrosClientes.Rows.Clear();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dgRegistrosClientes.Rows.Add(reader.GetString(0), reader.GetString(1),
                            reader.GetString(2), reader.GetString(3));
                    }
                }
                else
                {
                    MessageBox.Show("No se encontraron datos.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (dgRegistrosClientes.Rows.Count >= 50)
                MessageBox.Show("Se encontraron demasiados registros.\nNo se muestran todos los registros encontados.",
                    "Debes depurar la búsqueda");
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Agregar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "INSERT INTO clientes VALUES(NULL,'" + txtNombre.Text + "','" + txtDireccion.Text
                    + "','" + txtCelular.Text + "')";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
        }

        private void FrmClientes_Load(object sender, EventArgs e)
        {
            dgRegistrosClientes.Columns.Add("id_cliente", "ID Cliente");
            dgRegistrosClientes.Columns.Add("nombre", "Nombre");
            dgRegistrosClientes.Columns.Add("direccion", "Dirección");
            dgRegistrosClientes.Columns.Add("celular", "Celular");
        }

        private void dgRegistrosClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIdCliente.Text = dgRegistrosClientes.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgRegistrosClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtDireccion.Text = dgRegistrosClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCelular.Text = dgRegistrosClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        public void limpiarForms()
        {
            txtIdCliente.Text = "";
            txtCelular.Text = "";
            txtDireccion.Text = "";
            txtNombre.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //Eliminar
                string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
                string query = "DELETE FROM clientes WHERE id_cliente=" + txtIdCliente.Text;
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader;
                limpiarForms();

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
            }
            else MessageBox.Show("Error Cliente NO eliminado ");
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Modificar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "UPDATE clientes SET nombre='"
                + txtNombre.Text.Trim() + "', direccion='"
                + txtDireccion.Text.Trim() + "', celular='"
                + txtCelular.Text.Trim()
                + "' WHERE id_cliente=" + txtIdCliente.Text;
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
        }
    }
}
