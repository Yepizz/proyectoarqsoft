﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ComputerSales
{
    public partial class FrmReporteVentas : Form
    {
        public FrmReporteVentas()
        {
            InitializeComponent();
        }
        string archivo = Directory.GetCurrentDirectory() + "\\ReporteLaptops.htm";

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            StreamWriter arch = new StreamWriter(archivo);
            arch.WriteLine("<html><h2>Reporte de ventas</h2>");
            arch.WriteLine("<table border=1 cellspacing=0>");
            arch.WriteLine("<tr bgcolor=#9fb3f5><td>Id Venta</td><td>Id Cliente</td><td>Id Laptop</td><td>Fecha de venta</td><td>Cantidad</td><td>Precio de venta</td></tr>");
            //Buscar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "SELECT * FROM ventas";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        arch.WriteLine("<tr><td>"+ reader.GetString(0)
                                   + "</td><td>" + reader.GetString(1)
                                   + "</td><td>" + reader.GetString(2)
                                   + "</td><td>" + reader.GetString(3)
                                   + "</td><td>" + reader.GetString(4)
                                   + "</td><td>" + reader.GetString(5) 
                                   + "</td></tr>");
                    }
                }
                else
                {
                    MessageBox.Show("No se encontraron datos.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            arch.WriteLine("<table><html>");
            arch.Close();
            Uri dir = new Uri(archivo);
            webBrowser1.Url = dir;                                  
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            //Cierra la ventana actual
            if (MessageBox.Show("¿Desea salir?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FrmReporteVentas_Load(object sender, EventArgs e)
        {
            btnGenerar_Click(sender, e);
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("excel","\""+archivo+ "\"");
        }

        private void btnChrome_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("chrome", "\"" + archivo + "\"");
        }
    }
}
