﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComputerSales
{
    public partial class FrmComputerSales : Form
    {
        public FrmComputerSales()
        {
            InitializeComponent();
        }

        private void FrmComputerSales_Load(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //Cierra la ventana actual
            if (MessageBox.Show("¿Desea salir?", "Advertencia", MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de ventas
            FrmVentas frmVentas = new FrmVentas();
            //Tomamos el objeto y mostramos la ventana
            frmVentas.Show();
        }


        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de Clientes
            FrmClientes frmClientes = new FrmClientes();
            //Tomamos el objeto y mostramos la ventana
            frmClientes.Show();
        }

        private void reportesDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de Reporte de ventas
            FrmReporteVentas frmReporteVentas = new FrmReporteVentas();
            //Tomamos el objeto y mostramos la ventana
            frmReporteVentas.Show();
        }

     

        private void reporteDeClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de Reporte de Clientes
            FrmReporteClientes frmReporteClientes = new FrmReporteClientes();
            //Tomamos el objeto y mostramos la ventana
            frmReporteClientes.Show();
        }

        private void reporteDeLaptopsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de Reporte de Laptops
            FrmReporteLaptops frmReporteLaptops = new FrmReporteLaptops();
            //Tomamos el objeto y mostramos la ventana
            frmReporteLaptops.Show();
        }

        private void laptopsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creamos la instancia del formulario de Laptops
            FrmLaptops frmLaptops = new FrmLaptops();
            //Tomamos el objeto y mostramos la ventana
            frmLaptops.Show();
        }
    }
}
