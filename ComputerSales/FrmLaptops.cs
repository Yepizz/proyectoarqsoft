﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComputerSales
{
    public partial class FrmLaptops : Form
    {
        public FrmLaptops()
        {
            InitializeComponent();
        }

        private void FrmLaptops_Load(object sender, EventArgs e)
        {
            dgRegistrosLaptops.Columns.Add("id_laptop", "ID Laptop");
            dgRegistrosLaptops.Columns.Add("nombre", "Nombre");
            dgRegistrosLaptops.Columns.Add("precio", "Precio");
            dgRegistrosLaptops.Columns.Add("marca", "Marca");
            dgRegistrosLaptops.Columns.Add("stock", "Stock");
        }

        public void limpiarForms()
        {
            txtIdLaptop.Text = "";
            txtMarca.Text = "";
            txtPrecio.Text = "";
            txtStock.Text = "";
            txtNombre.Text = "";
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //Buscar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "SELECT * FROM laptops where nombre like '%" + txtBuscar.Text.Trim() + "%' limit 50";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            dgRegistrosLaptops.Rows.Clear();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dgRegistrosLaptops.Rows.Add(reader.GetString(0), reader.GetString(1),
                            reader.GetString(2), reader.GetString(3), reader.GetString(4));
                    }
                }
                else
                {
                    MessageBox.Show("No se encontraron datos.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (dgRegistrosLaptops.Rows.Count >= 50)
                MessageBox.Show("Se encontraron demasiados registros.\nNo se muestran todos los registros encontados.",
                    "Debes depurar la búsqueda");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            //Cierra la ventana actual
            if (MessageBox.Show("¿Desea salir?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Agregar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "INSERT INTO laptops VALUES(NULL,'" + txtNombre.Text + "','" + txtPrecio.Text 
                    + "','" + txtMarca.Text + "','" + txtStock.Text + "')";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
        }

        private void dgRegistrosLaptops_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIdLaptop.Text = dgRegistrosLaptops.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgRegistrosLaptops.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPrecio.Text = dgRegistrosLaptops.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtMarca.Text = dgRegistrosLaptops.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtStock.Text = dgRegistrosLaptops.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //Modificar
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "UPDATE laptops SET nombre='"
                + txtNombre.Text.Trim() + "', precio='"
                + txtPrecio.Text.Trim() + "', marca='"
                + txtMarca.Text.Trim()
                + "', stock='" + txtStock.Text.Trim()
                + "' WHERE id_laptop=" + txtIdLaptop.Text;
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que desea eliminar?", "Borrar Laptop", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //Eliminar
                string connectionString = "datasource=localhost;port=3306;username=root;password=;database=laptops_db;SSL Mode=None";
            string query = "DELETE FROM laptops WHERE id_laptop=" + txtIdLaptop.Text;
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            MySqlDataReader reader;
            limpiarForms();
            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            btnBuscar_Click(sender, e); //Buscar
            }
            else MessageBox.Show("Error Laptop NO eliminada");

        }
    }
}
